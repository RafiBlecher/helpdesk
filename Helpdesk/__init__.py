#!/usr/bin/python

from flask import (
  flash,
  Flask,
  g,
  render_template,
  request,
)

from flask.ext import login
from flask.ext.sqlalchemy import SQLAlchemy
from werkzeug.contrib.fixers import ProxyFix


class ReverseProxied(object):
    '''Wrap the application in this middleware and configure the
    front-end server to add these headers, to let you quietly bind
    this to a URL other than / and to an HTTP scheme that is
    different than what is used locally.

    In nginx:
    location /myprefix {
        proxy_pass http://127.0.0.1:5000;
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Scheme $scheme;
        proxy_set_header X-Script-Name /helpdesk;
        }

    :param app: the WSGI application
    '''
    def __init__(self, this_app):
        self.app = this_app

    def __call__(self, environ, start_response):
        script_name = environ.get('HTTP_X_SCRIPT_NAME', '')
        if script_name:
            environ['SCRIPT_NAME'] = script_name
            path_info = environ['PATH_INFO']
            if path_info.startswith(script_name):
                environ['PATH_INFO'] = path_info[len(script_name):]

        scheme = environ.get('HTTP_X_SCHEME', '')
        if scheme:
            environ['wsgi.url_scheme'] = scheme

        server = environ.get('HTTP_X_FORWARDED_SERVER', '')
        if server:
            environ['HTTP_HOST'] = server

        return self.app(environ, start_response)

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///datastore.db'
app.config['SECRET_KEY'] = 'such a secret key'

db = SQLAlchemy(app)

# To use `break` in templates
app.jinja_env.add_extension('jinja2.ext.loopcontrols')
# Get real remote IP, which is fiddled with by nginx's proxy-pass
app.wsgi_app = ProxyFix(app.wsgi_app)

# From http://flask.pocoo.org/snippets/35/
# I might love Peter Hansen
# This lets you run a flask app on some URL path other than / on a webserver.

app.wsgi_app = ReverseProxied(app.wsgi_app)

import models
from models import Company, User
from Helpdesk import views
from collections import OrderedDict

login_manager = login.LoginManager()
login_manager.init_app(app)
login_manager.user_loader(models.User.get_by_id)
login_manager.login_view = 'Login:index'
login_manager.login_message = 'You must be signed in to see this page.'
login_manager.login_message_category = 'warning'

from flask.ext.principal import Principal
principals = Principal(app)

views.register_views()


def add_user_logins():
  for name, email, mobile, pw, is_hashed in (
      ('Joe Blogs', 'joe@joeblogs.com', '0411 111 111', 'my_password',
       False),
      ('Jane Doe', 'jane@superawesomedomain.com.au', '0422 222 222',
       '$2a$12$HW7hpPkqW7ZLdbRWxRVgnueshMFuHC7bUoseQOPCo7Gh6mE0F.Sda', True)):
    if not User.query.filter_by(name=name).first():
      user = User(name, pw,
        mobile=mobile, email=email, hashed=is_hashed, admin=False)
      db.session.add(user)
      db.session.commit()


def add_admin_logins():
  if not User.query.filter_by(name='admin').first():
    name, pw, is_hashed = ('admin',
                '$12$VgsA14SO9liI8aimCqQBeEce5sNDKlK1rDTU1b35dNB5pnOMGmL6',
                True)
    admin = User(name, pw, email=None, hashed=is_hashed, admin=True)
    db.session.add(admin)
    db.session.commit()


def add_company_emails():
  for company, ip_prefix in (
      ('a', '255.255.255.'),
      ('b', '255.255.255.'),
      ('c', '255.255.255.'),
      ('d', '255.255.255.'),
      ('e', '255.255.255.'),
      ('f', '255.255.255.'),
      ('g', '255.255.255.'),
      ('h', '255.255.255.'),
      ('i', '255.255.255.'),
      ('j', '255.255.255.'),
      ('k', '255.255.255.')):
    email = 'company_contact_email@company.com'

    if not Company.query.filter_by(company_name=company).first():
      company = Company(company, email, ip_prefix)
      db.session.add(company)
      db.session.commit()


def choose_navbar_pages():
  allowed_pages = OrderedDict([
      ('New Request', 'MainPage:index'),
      ('Track Request', 'TrackRequest:index'),
      ])

  if login.current_user.is_anonymous():
    return allowed_pages

  allowed_pages = OrderedDict([
    ('New Request', 'MainPage:index'),
    ('Track Request', 'TrackRequest:index'),
    ('Closed Requests', 'ViewClosedRequests:index'),
    ('Reports', 'Reports:index'),
    ('My Next Task', 'MyNextTask:index'),
    ('Log Out', 'Logout:index')
    ])

  if login.current_user.is_admin:
    allowed_pages.update({'Edit Technicians': 'EditTechnicians:index'})

  return allowed_pages


@app.errorhandler(403)
def authorisation_failed(e):
    if g.__dict__.get('csrf_failed'):
        return render_template('errors/4/403/csrf.html'), 403
    else:
        current = User.query.get(g.identity.id)
        if current:
            flash(('Your current identity is {id}. You need special privileges to'
                   ' access this page').format(id=current.name), 'warning')
        else:
            flash('You are not currently signed in. You need special privileges to'
                  ' access this page', 'warning')

        return render_template('errors/4/403/privileges.html'), 403


@app.errorhandler(404)
def page_not_found(e):
  ''' Custom 404 page. Muuuuch prettier. '''
  return render_template('404.html'), 404


@app.before_first_request
def init_db():
  db.create_all()

  add_company_emails()
  add_user_logins()
  add_admin_logins()


@app.before_request
def before_request():
  g.navbar_pages = choose_navbar_pages()
  g.active_page = request.path

  g.companies = Company.query.all()
  g.categories = ('I have a problem!', 'My computer exploded!',
    'My keyboard melted!')
