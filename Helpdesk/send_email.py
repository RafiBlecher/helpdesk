import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


def send_email(to, subject, text, html, cc=None):
  msg = MIMEMultipart('alternative')
  msg['Subject'] = subject
  msg['From'] = 'helpdesk@mydomain'
  msg['To'] = to
  if cc:
    msg['Cc'] = cc

  part1 = MIMEText(text.encode('utf-8'), 'plain')
  part2 = MIMEText(html.encode('utf-8'), 'html')

  msg.attach(part1)
  msg.attach(part2)

  s = smtplib.SMTP('localhost')

  if cc:
    s.sendmail(msg['From'], [msg['To'], msg['Cc']], msg.as_string())
  else:
    s.sendmail(msg['From'], msg['To'], msg.as_string())

  s.quit()
