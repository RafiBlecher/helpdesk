from flask_principal import Permission as Require
from collections import namedtuple

# Define grantable permissions
Permission = namedtuple('Permission', ['method',
                                       'description'])

admin = Permission('admin', 'Can do admin things')

be_admin = Require(admin)
