import bcrypt

from Helpdesk import db


class User(db.Model):
  __tablename__ = 'users'

  id = db.Column(db.Integer, primary_key=True)

  name = db.Column(db.String(100))
  password = db.Column(db.String(256))
  email = db.Column(db.String(64))
  mobile = db.Column(db.String(30))
  is_admin = db.Column(db.Boolean())

  requests = db.relationship('Request',
    backref=db.backref('technician', lazy='joined'))

  def __init__(self, name, password, mobile=None, email=None, admin=False,
      hashed=True):
    self.name = name
    if not hashed:
      password = bcrypt.hashpw(password, bcrypt.gensalt())
    self.password = password
    self.email = email
    self.mobile = mobile
    self.is_admin = admin

  def is_authenticated(self):
    return True

  def is_active(self):
    return True

  def is_anonymous(self):
    return False

  def get_id(self):
    return unicode(self.id)

  @classmethod
  def get_by_id(cls, id):
    return cls.query.get(id)
