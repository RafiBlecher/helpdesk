from Helpdesk import db


class Company(db.Model):
  __tablename__ = 'companies'

  id = db.Column(db.Integer, primary_key=True)

  company_name = db.Column(db.String(100))
  company_email = db.Column(db.String(64))

  ip_prefix = db.Column(db.String(20))

  requests = db.relationship('Request',
    backref=db.backref('company', lazy='joined'))

  def __init__(self, name, email, ip_prefix):
    self.company_name = name
    self.company_email = email
    self.ip_prefix = ip_prefix

  @classmethod
  def get_by_ip(cls, ip_address):
    for company in cls.query.all():
      if ip_address.startswith(company.ip_prefix):
        return company
