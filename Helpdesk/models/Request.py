from Helpdesk import db
from Helpdesk.models.Company import Company
from Helpdesk.models.User import User


class Request(db.Model):
  __tablename__ = 'requests'

  id = db.Column(db.Integer, primary_key=True)

  ticket_id = db.Column(db.String(10))  # 10DEC13

  ip_address = db.Column(db.String(20))  # 255.255.255.255

  full_name = db.Column(db.String(100))
  email = db.Column(db.String(64))

  company_id = db.Column(db.Integer(), db.ForeignKey('companies.id'))

  category = db.Column(db.String(50))
  severity = db.Column(db.String(20))

  status = db.Column(db.String(10))  # 'Open' or 'Closed'

  description = db.Column(db.String())  # too scared to give this a length

  email_update = db.Column(db.Boolean())
  releasecall = db.Column(db.Boolean())

  date_submitted = db.Column(db.Numeric())
  estimated_time = db.Column(db.String(100))
  date_closed = db.Column(db.Numeric())
  resolution = db.Column(db.String())  # too scared to give this a length

  technician_id = db.Column(db.Integer(), db.ForeignKey('users.id'))

  estimated_difficulty = db.Column(db.String(50))
  paused = db.Column(db.Boolean())
  unpaused_ETA = db.Column(db.String(100))
  paused_reason = db.Column(db.String())  # too scared to give this a length

  update_info = db.Column(db.String())  # too scared to give this a length

  def __init__(self, form):
    # dirty hacks are okay with me!
    for k, v in form.iteritems():
      if k == 'company':
        self.company = Company.query.filter_by(company_name=v).first()
      elif k == 'technician':
        self.technician = User.query.filter_by(name=v).first()
      else:
        self.__dict__[k] = v
    for k in ('paused', 'email_update', 'releasecall'):
      self.__dict__[k] = 1 if k in form else 0
