from flask.ext import login
from collections import OrderedDict
from Helpdesk.views.PageType import ProtectedPage
from Helpdesk.models import Request, User

from flask import (
  render_template,
)


class MyNextTask(ProtectedPage):
  route_base = '/my_next_task'

  def index(self):
    tables = OrderedDict([])
    tables.update({'Open': Request.query.filter_by(
      technician=None, status='Open').all()})

    technicians = sorted(
      [u for u in User.query.filter_by(is_admin=False).all()],
      key=lambda x: x.name)

    current_user = login.current_user
    # move current_user to front of list
    if not current_user.is_admin:
      technicians.remove(current_user)
      technicians.insert(0, current_user)
    for technician in technicians:
      current_requests = sorted(
        [x for x in technician.requests if x.status == 'Open'],
        key=lambda r: (r.paused,
          ['Easy', 'Medium', 'Hard'].index(r.estimated_difficulty),
          r.date_submitted
        ))
      tables.update({technician: current_requests})

    return render_template('my_next_task.html', tables=tables)
