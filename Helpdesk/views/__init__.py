from flask.ext.principal import (
  identity_changed,
  identity_loaded,
  UserNeed,
)

from flask.ext.login import current_user

from Helpdesk.permissions_and_requirements import admin

from Helpdesk import app

from main_page import MainPage

from login import Login
from logout import Logout

from register import Register

from reports import Reports

from my_next_task import MyNextTask

from helpdesk_request import (
  TrackRequest,
  UpdateRequest,
  ProcessRequest,
  ViewClosedRequests
)

from edit_technicians import EditTechnicians


def register_views():
  Login.register(app)
  Logout.register(app)
  Reports.register(app)
  MainPage.register(app)
  Register.register(app)
  MyNextTask.register(app)
  TrackRequest.register(app)
  UpdateRequest.register(app)
  ProcessRequest.register(app)
  EditTechnicians.register(app)
  ViewClosedRequests.register(app)


@identity_changed.connect_via(app)
def on_identity_changed(sender, identity):
  on_identity_loaded(sender, identity)


@identity_loaded.connect_via(app)
def on_identity_loaded(sender, identity):
  # Give permissions when an identity is loaded.
  identity.provides = set()

  if not current_user.is_anonymous() and current_user.is_admin:
    identity.provides.add(admin)
