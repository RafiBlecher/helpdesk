from flask.ext import classy, login


class Page(classy.FlaskView):
  route_base = ''


class ProtectedPage(Page):
  decorators = [login.login_required]
