import bcrypt

from Helpdesk.views.PageType import Page

from flask.ext import login

from Helpdesk.models import User

from flask import (
  current_app,
  flash,
  g,
  redirect,
  render_template,
  request,
  url_for,
)

from flask.ext.principal import (
  Identity,
  identity_changed,
  identity_loaded,
)


class Login(Page):
  route_base = '/login'

  def index(self):
    return render_template('login.html')

  def post(self):
    u = User.query.filter_by(name=request.form['name']).first()
    if u and u.password == bcrypt.hashpw(
        request.form['password'].encode('utf-8'), u.password.encode('utf-8')):
      login.login_user(u)

      identity = Identity(u.id)
      identity_changed.send(current_app._get_current_object(),
                            identity=identity)
      g.identity = identity
      identity_loaded.send(current_app._get_current_object(),
                           identity=identity)

      return redirect(url_for('MyNextTask:index'))
    else:
      flash('Incorrect name or password', 'danger')
      return redirect(url_for('Login:index'))
