from Helpdesk import db
from math import ceil as ceiling
from flask.ext.classy import route
from Helpdesk.models import Request
from Helpdesk.views.PageType import ProtectedPage

from flask import (
  render_template,
  request,
)

RESULTS = 25


def score(matched):
  """ Returns score based on matched keywords """
  # 4** scalar rewards matching unique keywords instead of the same one
  # multiple times
  return len(matched) * (4 ** (len(set(matched)) - 1))


class ViewClosedRequests(ProtectedPage):
  route_base = '/search_closed'

  def index(self):
    requests = Request.query.filter_by(status='Closed').all()
    num = int(ceiling(len(requests) / float(RESULTS)))

    page = int(request.values.get('page', 1))

    return render_template('closed_requests/view_closed_requests.html',
      num=num, page_num=page,
      requests=requests)

  @route('search_closed_requests', endpoint='search_closed_requests')
  def get(self):
    page = int(request.values.get('page', 1))
    skip_num = RESULTS * (page - 1)
    search = request.values['search']

    results = {}
    for keyword in search.split():
        match_requests = Request.query.filter_by(status='Closed').filter(
          db.or_(db.func.lower(Request.description).like('%'+keyword.lower()+'%'),
                 db.func.lower(Request.resolution).like('%'+keyword.lower()+'%'))
        ).all()

        for r in match_requests:
          search_aggregate_string = (r.description + r.resolution).lower()
          results[r] = [keyword] * search_aggregate_string.count(keyword)

    results = sorted(results,
      key=lambda k: (score(results[k]), k.ticket_id), reverse=True)

    num = int(ceiling(len(results) / float(RESULTS)))

    return render_template('closed_requests/view_closed_requests.html',
      search=request.values['search'], num=num, page_num=page,
      requests=results[skip_num:skip_num+RESULTS])
