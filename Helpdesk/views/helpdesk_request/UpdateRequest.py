from time import time

from flask.ext import login

from Helpdesk import db
from Helpdesk.send_email import send_email
from Helpdesk.models import Request, Company, User
from Helpdesk.views.PageType import ProtectedPage

from flask import (
  redirect,
  request,
  session,
  url_for,
)


def email_tech_assigned_task(form):
  subject = "Ticket {ticket} assigned to you - Help Desk".format(ticket=form.get('ticket'))
  send_to = User.filter_by(name=form['technician']).email

  text = u"""
  Hi {technician},\n\n
  {logged_in} just assigned ticket {ticket} to you.\n\n
  Description:\n
  {description}\n\n
  View ticket here:
  http://192.168.2.11/helpdesk/show_request?ticket={ticket}.""".format(
    description=form.get('description'),
    technician=form.get('technician'),
    logged_in=User.get(session['user_id']).name,
    ticket=form.get('ticket')
  )

  html = u"""
<html>
  <head>
  </head>
  <body>
    Hi {technician},<br /><br />
    {logged_in} just assigned ticket {ticket} to you.<br /><br />
    Description:<br />
    {description}<br /><br />
    <a href='http://192.168.2.11/helpdesk/show_request?ticket={ticket}'>View ticket here</a>
  </body>
</html>""".format(technician=form.get('technician'), logged_in=User.get(session['user_id']).name,
                  ticket=form.get('ticket'), description=form.get('description'))

  send_email(send_to, subject=subject, text=text, html=html)


def email_request_update(form, company_email):
  subject = "Help Desk - Ref# " + form.get('ticket')

  text = u"""
  This is an automated response\n\n
  Thank you {name},\n\n
  This email is being sent to you to let you know that your ticket {ticket} has been updated.\n\n
  Your ticket is assigned to {technician}.\n\n
  Description:\n\n
  {description}\n\n
  Update:\n\n
  {update}\n\n
  Find your ticket here: http://192.168.2.11/helpdesk/show_request?ticket={ticket}\n\n
  Regards,\n
  Helpdesk System""".format(name=form.get('fullname'),
                     description=form.get('description'),
                     ticket=form.get('ticket'),
                     technician=form.get('technician'),
                     update=form.get('updatetext'))

  html = u"""
<html>
  <head>
  </head>
  <body>
    <b>This is an automated response</b><br /><br />
    Thank you {name},<br /><br />
    This email is being sent to you to let you know that your ticket {ticket} has been updated.<br /><br/>
    Your ticket is assigned to {technician}.<br /><br />
    <b>Description</b><br /><br />
    {description}<br /><br />
    <b>Update</b><br /><br />
    {update}<br /><br />
    <a href='http://192.168.2.11/helpdesk/show_request?ticket={ticket}'>Direct link to your ticket</a><br /><br />
    Regards,<br />
    Helpdesk System
  </body>
</html>
""".format(name=form.get('fullname'), description=form.get('description'),
       ticket=form.get('ticket'), technician=form.get('technician'),
       update=form.get('updatetext'))

  send_email(form.get('email'), subject=subject, text=text, html=html, cc=company_email)


def email_request_closed(form):
  subject = "Help Desk - Ref# " + form.get('ticket')

  text = u"""
  This is an automated message\n\n
  Hi {name},\n\n
  This email is being sent to you to let you know that your ticket {ticket} has been resolved and closed.\n\n
  {description}\n\n
  Regards,\n
  Helpdesk System""".format(name=form.get('fullname'), description=form.get('description'), ticket=form.get('ticket'))

  html = u"""
<html>
  <head>
  </head>
  <body>
    <b>This is an automated message</b><br /><br />
    Hi {name},<br /><br />
    This email is being sent to you to let you know that your ticket {ticket} has been resolved and closed.<br /><br/>
    {description}<br /><br />
    Regards,<br />
    Helpdesk System
  </body>
</html>
""".format(name=form.get('fullname'), description=form.get('description'), ticket=form.get('ticket'))

  send_email(form.get('email'), subject=subject, text=text, html=html)


class UpdateRequest(ProtectedPage):
  route_base = '/update_request'

  def post(self):
    helpdesk_request = Request.query.filter_by(
      ticket_id=request.values['ticket_id']).first()
    if request.form['action'] == 'delete':
      db.session.delete(helpdesk_request)
      db.session.commit()
    else:
      # dirty hacks are okay with me!
      form = {k: v for k, v in request.form.iteritems()}
      form.pop('action')
      for k, v in form.iteritems():
        if k not in helpdesk_request.__dict__:
          continue
        if k == 'company':
          helpdesk_request.company = \
            Company.query.filter_by(company_name=v).first()
        elif k == 'technician':
          helpdesk_request.technician = \
            User.query.filter_by(name=v).first()
        else:
          form[k] = v
      form.pop('company')
      form.pop('technician')
      for k in ('paused', 'email_update', 'releasecall'):
        form[k] = k in form
      if 'paused' not in form:
        form['paused_reason'] = ''
        form['unpaused_ETA'] = ''

      if form.get('status') == 'Closed' and not helpdesk_request.date_closed:
          # ticket is newly closed
          form['date_closed'] = time()
      if form.get('status') == 'Open' and helpdesk_request.date_closed:
          # ticket in newly reopened
          form['date_closed'] = 0.0

      if 'technician' in form and \
          form.get('technician') != helpdesk_request.technician:
        if form['technician'] != login.current_user.name:
          email_tech_assigned_task(form)

      if form.get('emailupdate') == 'on':
        if form.get('status') == 'Closed' and \
            helpdesk_request.status != 'Closed':
          email_request_closed(form)
        else:
          email_request_update(form, helpdesk_request.company.company_email)

      Request.query.filter_by(id=helpdesk_request.id).update(form)
      db.session.commit()
    return redirect(url_for('MyNextTask:index'))
