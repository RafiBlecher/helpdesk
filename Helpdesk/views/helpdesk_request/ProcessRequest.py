import sys
import traceback

from time import localtime, strftime, time

from Helpdesk import db
from Helpdesk.send_email import send_email
from flask.ext.classy import route
from Helpdesk.models import Request
from Helpdesk.views.PageType import Page

from flask import (
  redirect,
  render_template,
  request,
  session,
  url_for,
)


def email_request_submitted(form, company_email):
  text = u"""
  This is an automated response\n\n
  Thank you {name},\n\n
  The information you submitted is currently being processed with the following description:\n\n
  {description}\n\n
  and severity: {severity}\n\n
  Find your ticket here: http://192.168.2.11/helpdesk/show_request?ticket={ticket}\n\n
  Regards,\n
  Helpdesk System""".format(name=form.get('fullname'),
                     description=form.get('description'),
                     severity=form.get('severity'),
                     ticket=form.get('ticket'))

  html = u"""
<html>
  <head>
  </head>
  <body>
    <b>This is an automated response</b><br /><br />
    Thank you {name},<br /><br />
    The information you submitted is currently being processed with the following description:<br /><br/>
    {description}<br /><br />
    and severity: {severity}<br /><br />
    <a href='http://192.168.2.11/helpdesk/show_request?ticket={ticket}'>Direct link to your ticket</a><br /><br />
    Regards,<br />
    Helpdesk System
  </body>
</html>
""".format(name=form.get('fullname'), description=form.get('description'), severity=form.get('severity'), ticket=form.get('ticket'))

  send_email(form['email'],
    subject='Help Desk - Ref # ' + form['ticket_id'],
    text=text,
    html=html,
    cc=company_email)


class ProcessRequest(Page):

  @route('/process_request', endpoint='process_request', methods=['POST'])
  def process_request(self):
    # TODO (RB) voluptuous-auth the form
    try:
      request_form = {k: v for k, v in request.form.iteritems()}
      request_form['ip_address'] = request.remote_addr

      date = strftime('%d%b%y', localtime()).upper()  # looks like 10DEC13
      result = db.session.query(db.func.max(Request.ticket_id)).filter(
        Request.ticket_id.like('%'+date)).first()
      result = result[0]  # really? :P
      ticket_num = 1
      if result and result[5:] == date:
        last_ticket_number = int(result[:4])
        ticket_num = last_ticket_number + 1
      # Turns 1 and 10DEC13 into 0001-10DEC13
      ticket_id = '{num}-{date}'.format(num=ticket_num, date=date).zfill(12)
      request_form['ticket_id'] = ticket_id

      request_form['date_submitted'] = time()
      request_form['status'] = 'Open'

      new_request = Request(request_form)
      db.session.add(new_request)
      db.session.commit()

      ticket = new_request.ticket_id
      company_email = new_request.company.company_email

      email_request_submitted(request_form, company_email)

      session['ticket'] = (ticket, company_email, request_form['full_name'])
      return redirect(url_for('form_accepted'))
    except:
      # TODO Maybe send off an email to helpdesk with the error as well?
      e = sys.exc_info()[0]
      e_traceback = []
      for tb in traceback.format_tb(sys.exc_info()[2]):
        e_traceback += tb.split('\n')
      return render_template('request_submit_error.html', e=e,
        e_traceback=e_traceback, form=request.values)

  @route('/thank_you', endpoint='form_accepted')
  def form_accepted(self):
    ticket, email, fullname = session.pop('ticket')
    return render_template('thank_you.html', ticket=ticket, fullname=fullname,
      email=email)
