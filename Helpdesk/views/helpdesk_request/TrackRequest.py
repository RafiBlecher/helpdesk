from Helpdesk.models import Request
from Helpdesk.views.PageType import Page

from flask import (
  flash,
  redirect,
  render_template,
  request,
  url_for,
)


def find_ticket(ticket_id):
  helpdesk_request = Request.query.filter_by(
    ticket_id=ticket_id).first()
  if helpdesk_request:
    responsetime = None
    if helpdesk_request.date_closed:
      date_closed = helpdesk_request.date_closed
      date_submitted = helpdesk_request.date_submitted
      responsetime = (date_closed - date_submitted) / 60  # to minutes
    return render_template('track_request/view_request.html',
      responsetime=responsetime, **helpdesk_request.__dict__)
  else:
    flash("That ticket doesn't exist!", 'danger')
    return redirect(url_for('TrackRequest:index'))


class TrackRequest(Page):
  route_base = '/track_request'

  def index(self):
    return render_template('track_request/track_request.html')

  def get(self, ticket_id):
    return find_ticket(ticket_id)

  def post(self):
    return find_ticket(request.values['ticket'])
