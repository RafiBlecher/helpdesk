from datetime import datetime, timedelta
from collections import defaultdict
from time import localtime, mktime, strftime, time

from Helpdesk.models import Request

from Helpdesk.views.PageType import ProtectedPage

from flask import (
  render_template,
  request,
)


def report_stats(requests, companies, start_date, end_date):
  # TODO make the beautiful Hannah fix your shit variable names.
  stats = defaultdict(dict)
  for r in requests:
    company, category, status, date_submitted, date_closed, ETA, reason \
      = [r.__dict__[i]
         for i in ('company', 'category', 'status', 'date_submitted',
             'date_closed', 'unpaused_ETA', 'paused_reason')]
    company = company.company_name
    if company not in companies:
      continue

    overall_total = stats.get('Totals', defaultdict(int))
    overall_total['total'] += 1
    overall_total[status] += 1
    stats['Totals'] = overall_total

    r_stats = stats[company]
    r_stats['totals'] = r_stats.get('totals', defaultdict(int))
    r_stats['totals']['total'] += 1
    r_stats['totals'][status] += 1

    r_stats[category] = r_stats.get(category, {})
    r_stats[category]['reasons'] = r_stats[category].get('reasons', [])
    if reason or ETA:
      r_stats[category]['reasons'].append((ETA, reason))
    r_stats[category]['total'] = r_stats[category].get('total', 0) + 1
    if date_closed:
      time_taken = (date_closed - date_submitted) / 60  # in minutes
      if time_taken < 60:
        r_stats[category]['0-60 min'] = \
          r_stats[category].get('0-60 min', 0) + 1
      elif time_taken < 24 * 60:
        r_stats[category]['1hr-24hr'] = \
          r_stats[category].get('1hr-24hr', 0) + 1
      else:
        r_stats[category]['Over 24hr'] = \
          r_stats[category].get('Over 24hr', 0) + 1
    else:
      r_stats[category]['Open Tickets'] = \
        r_stats[category].get('Open Tickets', 0) + 1
    stats[company] = r_stats
  return stats


class Reports(ProtectedPage):
  route_base = 'reports'

  def index(self):
    curr_time = time()
    day = 24 * 60 * 60
    yesterday = curr_time - day

    today = localtime(curr_time)

    # TODO comment this you lazy ass
    dt = datetime.fromtimestamp(curr_time)
    dt += timedelta(hours=23 - today.tm_hour,
                    minutes=59 - today.tm_min,
                    seconds=59 - today.tm_sec)
    today = mktime(dt.timetuple())
    return render_template('reports/reports.html',
      yesterday=strftime('%d/%m/%Y', localtime(yesterday)),
      today=strftime('%d/%m/%Y', localtime(today)))

  def post(self):
    start_date = mktime(datetime.strptime(request.form['startdate'],
      '%d/%m/%Y').timetuple())
    end_date = datetime.strptime(request.form['enddate'], '%d/%m/%Y')
    end_date += timedelta(hours=23, minutes=59, seconds=59)
    end_date = mktime(end_date.timetuple())

    companies_to_show = [k for k, v in request.form.iteritems() if v == 'on']

    stats = report_stats(
      Request.query.filter(Request.date_submitted >= start_date).
        filter(Request.date_submitted <= end_date).all(),
      companies_to_show, start_date, end_date)

    return render_template('reports/report.html', start_date=start_date,
      end_date=end_date, stats=stats)
