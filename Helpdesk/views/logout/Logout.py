from Helpdesk.views.PageType import ProtectedPage

from flask.ext import login

from flask import (
  redirect,
  request,
  url_for,
)


class Logout(ProtectedPage):
  route_base = '/logout'

  def index(self):
    login.logout_user()
    return redirect(request.args.get('next') or url_for('MainPage:index'))
