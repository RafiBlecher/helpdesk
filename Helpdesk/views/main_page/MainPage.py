from Helpdesk import db
from Helpdesk.models import Company, User

from time import strftime, localtime
from Helpdesk.views.PageType import Page

from flask import (
  render_template,
  request,
)


class MainPage(Page):
  route_base = '/'

  def index(self):
    ip_address = request.remote_addr
    company = Company.get_by_ip(ip_address)
    company_name = company.company_name if company else None
    users = User.query.filter(db.not_(User.is_admin)).all()
    return render_template('main_page.html', ip_address=ip_address,
      current_time=strftime('%d/%m/%Y', localtime()), users=users,
      company=company_name)
