from Helpdesk.views.PageType import ProtectedPage

from Helpdesk.permissions_and_requirements import be_admin

from Helpdesk import db
from Helpdesk.models import User

from flask import (
  # current_app,
  # flash,
  # g,
  # redirect,
  render_template,
  # request,
  # url_for,
)

from flask.ext.login import login_required
from flask.ext.classy import route


class EditTechnicians(ProtectedPage):
  route_base = '/edit_technicians'
  decorators = [login_required, be_admin.require(403)]

  def index(self):
    techs = User.query.filter(db.not_(User.is_admin)).all()
    return render_template('edit_technicians/manage.html', technicians=techs)

  def edit(self, id):
    return render_template('edit_technicians/edit.html',
      technician=User.query.get(int(id)))
