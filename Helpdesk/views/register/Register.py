import bcrypt

from Helpdesk import db
from Helpdesk.models import User
from Helpdesk.views.PageType import Page

from flask import (
  redirect,
  request,
  url_for,
)


class Register(Page):
  route_base = '/register'

  def post(self):
    # TODO (RB) voluptuous-validate this form
    pw = bcrypt.hashpw(request.form['password'].encode('utf-8'),
      bcrypt.gensalt())
    u = User(request.form['name'], pw, request.form.get('mobile'),
      request.form['email'], admin=False)
    db.session.add(u)
    db.session.commit()
    return redirect(request.args.get('next') or
                    url_for('EditTechnicians:index'))
