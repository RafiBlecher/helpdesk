#!/usr/bin/python

import sys

from Helpdesk import app

debug = '--debug' in sys.argv
app.run(host='0.0.0.0', debug=debug)
